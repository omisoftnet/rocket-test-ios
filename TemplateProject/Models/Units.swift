//
//  Units.swift
//  TemplateProject
//
//  Created by Руслан Федорович on 7/3/19.
//  Copyright © 2019 omisoft. All rights reserved.
//

import Foundation

enum Units: Int {
    case Kilograms = 0, Grams, Case, Custom

    var title: String? {
        switch self {
        case .Kilograms:
            return "Kg"
        case .Grams:
            return "Grams"
        case .Case:
            return "Case"
        default:
            return nil
        }
    }
}
