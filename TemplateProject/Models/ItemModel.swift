//
//  ItemModel.swift
//  
//
//  Created by Руслан Федорович on 7/3/19.
//

import Foundation

struct ItemModel {
    var title: String
    var units: String
}
