//
// Created by eugene on 12.08.18.
// Copyright (c) 2018 omisoft. All rights reserved.
//

import Foundation
import RxSwift

class MvpPresenter<V>: Presenter<V> {

    private lazy var subscriptions = CompositeDisposable()

    override func detachView() {
        super.detachView()

        unsubscribeAll()
    }
    
    func addSubscription(subscription: Disposable) {
        _ = subscriptions.insert(subscription)
    }

    private func unsubscribeAll() {
        subscriptions.dispose()
    }
    
}
