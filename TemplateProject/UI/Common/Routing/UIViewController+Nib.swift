//
//  UIViewController+Nib.swift
//  TemplateProject
//
//  Created by Dmytro Romaniuk on 9/14/18.
//  Copyright © 2018 omisoft. All rights reserved.
//

import UIKit

extension UIViewController {
    class func loadFromNib<T: UIViewController>() -> T {
        let name = String(describing: self)
        return T(nibName: name, bundle: nil)
    }
}
