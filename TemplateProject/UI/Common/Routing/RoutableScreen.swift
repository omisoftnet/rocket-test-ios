//
//  RoutableVC.swift
//
//  Created by Dmytro Romaniuk on 9/10/18.
//  Copyright © 2018 omisoft. All rights reserved.
//

import Foundation

protocol RoutableScreen {
    var context: RouteContext? { get set }
}
