//
// Created by eugene on 12.08.18.
// Copyright (c) 2018 omisoft. All rights reserved.
//
import UIKit

protocol MvpView {
    func startLoading()
    func finishLoading()
    
    func showMessage(message: AppString)
    func showMessage(message: String)
    func showMessage(title: String?, message: String, alertActions: [UIAlertAction]?)
    
    func openScreen(fromStoryboard: Storyboard, withName: String,
                    andContext: RouteContext?)
    func openChildScreen(fromStoryboard: Storyboard, withName: String,
                         andContext: RouteContext?)
    func backToPrevScreen(with context: RouteContext?)
}
