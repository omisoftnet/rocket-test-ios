//
//  Presenter.swift
//  TemplateProject
//

import Foundation

class Presenter<V> {

    var mvpView: V?
    var context: RouteContext?
    
    func attachView(mvpView: V) {
        self.mvpView = mvpView
    }
    
    func setContext(to context: RouteContext?) {
        self.context = context
    }
    
    func detachView() {
        mvpView = nil
    }
}
