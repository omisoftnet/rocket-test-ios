//
//  TagListView+Style.swift
//  TemplateProject
//
//  Created by Dmytro Romaniuk on 9/14/18.
//  Copyright © 2018 omisoft. All rights reserved.
//

import Foundation
import TagListView

struct TagStyle {
    
    static func font(_ font: UIFont) -> Decoration<TagListView> {
        return {
            (view: TagListView) -> Void in
            view.textFont = font
        }
    }
    
    static var fontTag: Decoration<TagListView> {
        return font(UIFont.systemFont(ofSize: 12))
    }
}
