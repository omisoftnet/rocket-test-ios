//
//  UILabel+Style.swift
//  TemplateProject
//

import Foundation
import UIKit

struct TextStyle {
    static func font(_ font: UIFont) -> Decoration<UILabel> {
        return {
            (view: UILabel) -> Void in
            view.font = font
        }
    }
    
    static var fontHeader: Decoration<UILabel> {
        return font(UIFont(name: ".SFUIText-Medium", size: 18)!)
    }
    
    static var fontCellHeader: Decoration<UILabel> {
        return font(UIFont(name: ".SFUIText-Medium", size: 14)!)
    }
}
