//
// Created by eugene on 12.08.18.
// Copyright (c) 2018 omisoft. All rights reserved.
//

import Foundation
import UIKit

class BaseVC<P>: UIViewController, RoutableScreen {
    
    enum CalledActionObject {
        case finishLoading
        case startLoading
        case showMessage
    }
    
    var context: RouteContext? {
        didSet {
            if presenter != nil {
                setContext(context)
            }
        }
    }
    var presenter: P?
    
    var smallDisplay: Bool {
        if UIScreen.main.bounds.height <= 568 {
            return true
        } else {
            return false
        }
    }
    
    private var isLoadingShown: Bool = false
    private var calledAction: [CalledActionObject] = []
    private var showingMessage: () -> Void = {}
    private var indicator: UIAlertController?
    private var areActionsPerforming = false
    private var appName: String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName") as? String
            ?? "MVPTemplate"
    }
    
    private var indicatorBuilder: UIAlertController {
        let alert = UIAlertController(title: nil,
                message: AppString.pleaseWait.localized, preferredStyle: .alert)
        
        let loadingIndicator = UIActivityIndicatorView(frame:
                CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        loadingIndicator.startAnimating()
        
        alert.view.addSubview(loadingIndicator)
        
        return alert
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        initPresenter(with: context)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupStyle()
    }
    
    func initPresenter(with context: RouteContext?) {
        fatalError("Subclasses must implement initPresenter()")
    }

    func setContext(_ context: RouteContext?) {
        
    }
    
    func setupStyle() {
        if let navigationBar = navigationController?.navigationBar {
//            navigationBar <- [NavBarStyle.colorAzure,
//                              NavBarStyle.fontSemiBold17, NavBarStyle.tintColorWhite, NavBarStyle.textColorWhite]
            navigationBar.shadowImage = UIImage()
        }
    }
    
    // MARK: KEYBOARD
    @objc func keyboardWillShow(notification: NSNotification) { }
    
    @objc func keyboardWillHide(notification: NSNotification) { }
    
    func addKeyboardNotifications() {
        NotificationCenter.default
            .addObserver(self,
                         selector: #selector(keyboardWillShow(notification:)),
                         name: NSNotification.Name.UIKeyboardWillShow,
                         object: nil)
        NotificationCenter.default
            .addObserver(self,
                         selector: #selector(keyboardWillHide(notification:)),
                         name: NSNotification.Name.UIKeyboardWillHide,
                         object: nil)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    // MARK: Indicator
    func startLoading() {
        calledAction.append(.startLoading)
        if !areActionsPerforming {
            areActionsPerforming = true
            performNextAction()
        }
    }
    
    func finishLoading() {
        calledAction.append(.finishLoading)
        if !areActionsPerforming {
            areActionsPerforming = true
            performNextAction()
        }
    }
}

extension BaseVC: MvpView {
    
    // MARK: ROUTING
    
    func openScreen(fromStoryboard storyboard: Storyboard, withName screen: String,
                    andContext context: RouteContext? = nil) {
        Router(viewController: self)
            .openScreen(fromStoryboard: storyboard, withName: screen, andContext: context)
    }
    
    func openChildScreen(fromStoryboard storyboard: Storyboard, withName screen: String,
                         andContext context: RouteContext? = nil) {
        Router(viewController: self)
            .openChildScreen(fromStoryboard: storyboard, withName: screen, andContext: context)
    }
    
    func backToPrevScreen(with context: RouteContext? = nil) {
        Router(viewController: self).backToPrevScreen(with: context)
    }
    
    // MARK: MESSEGES AND INDICATION
    
    func performStartLoading() {
        guard !isLoadingShown else {
            self.performNextAction()
            return
        }
        indicator = indicatorBuilder
        present(indicator!, animated: true, completion: {
            self.isLoadingShown = true
            self.performNextAction()
        })
    }
    
    func performFinishLoading() {
        guard isLoadingShown else {
            self.performNextAction()
            return
        }
        self.indicator?.dismiss(animated: false, completion: {
            self.isLoadingShown = false
            self.performNextAction()
        })
    }
    
    func showMessage(message: AppString) {
        showMessage(message: message.localized)
    }
    
    func showMessage(message: String) {
        showMessage(title: nil, message: message, alertActions: nil)
    }
    
    func showMessage(title: String?, message: String, alertActions: [UIAlertAction]?) {
        calledAction.append(.showMessage)
        let title = title ?? appName
        let alertActions = alertActions ?? []
        showingMessage = {
            self.showCustomMessage(title: title, message: message, alertActions: alertActions)
        }
        if !areActionsPerforming {
            areActionsPerforming = true
            performNextAction()
        }
    }
    
    private func showCustomMessage(title: String, message: String, alertActions: [UIAlertAction]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alertActions.forEach({alertAction in alert.addAction(alertAction)})
        
        self.present(alert, animated: true)
    }
    
    private func performNextAction() {
        guard let nextAction = calledAction.first else {
            areActionsPerforming = false
            return
        }
        calledAction.removeFirst()
        switch nextAction {
        case .finishLoading:
            performFinishLoading()
        case .startLoading:
            performStartLoading()
        case .showMessage:
            showingMessage()
            areActionsPerforming = false
        }
    }
}
