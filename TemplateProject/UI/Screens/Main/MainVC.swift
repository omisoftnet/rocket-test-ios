//
//  ViewController.swift
//  TemplateProject
//

import UIKit

class MainVC: BaseVC<MainViewPresenter>, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var addItemStackView: UIStackView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var topAddItemButton: UIButton!
    @IBOutlet weak var addItemButton: UIButton!
    
    private var refreshControl: UIRefreshControl!
    private let cellHeight: CGFloat = 78

    override func initPresenter(with context: RouteContext?) {
        presenter = MainViewPresenter()
        presenter?.attachView(mvpView: self)
        presenter?.setContext(to: context)
    }

    override func setContext(_ context: RouteContext?) {
        presenter?.setContext(to: context)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.doOnStart()
    }

    override func setupStyle() {
        super.setupStyle()
        addItemButton <- [Style.cornersMedium]
        
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.doOnAppearing()
    }

    private func setupTableView() {
        let nib = UINib(nibName: ItemViewCell.identifier, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: ItemViewCell.identifier)
    }

    @IBAction func addItemButtonTouched(_ sender: Any) {
        presenter?.addItemButtonTouched()
    }
    // MARK: UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.items.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: ItemViewCell.identifier,
                                                    for: indexPath) as? ItemViewCell {
            cell.item = presenter?.items[indexPath.row]
            
            return cell
        } else {
            return ItemViewCell()
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension MainVC: MainMvpView {
    func setupView() {
        setupTableView()
    }
    
    func showAddItemView() {
        addItemStackView.isHidden = false
        topAddItemButton.isHidden = true

    }

    func hideAddItemView() {
        addItemStackView.isHidden = true
        topAddItemButton.isHidden = false
    }

    func updateItems() {
        tableView.reloadData()
    }
}
