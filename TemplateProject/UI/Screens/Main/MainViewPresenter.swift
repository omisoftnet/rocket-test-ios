//
//  File.swift
//  TemplateProject
//

import Foundation
import RxSwift

class MainViewPresenter: MvpPresenter<MainMvpView> {

    static let ArgItem = "ARG_ITEM"
    
    var items = [ItemModel]()

    override func setContext(to context: RouteContext?) {
        super.setContext(to: context)

        guard let context = context else { return }
        guard let item: ItemModel = context[MainViewPresenter.ArgItem] else {
            return
        }
        print(item)
        self.items.append(item)
    }

    func doOnStart() {
        mvpView?.setupView()
        updateItems()
    }

    func doOnAppearing() {
        updateItems()
    }

    private func updateItems() {
        if items.isEmpty {
            mvpView?.showAddItemView()
        } else {
            mvpView?.hideAddItemView()
        }
        mvpView?.updateItems()
    }

    func addItemButtonTouched() {
        mvpView?.openChildScreen(fromStoryboard: .main, withName: "AddItemVC", andContext: nil)
    }
}
