//
//  PostViewCellTableViewCell.swift
//  TemplateProject
//

import UIKit

class ItemViewCell: UITableViewCell {

    static let identifier = "ItemViewCell"

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var unitsLabel: UILabel!

    var item: ItemModel? {
        didSet {
            setupData()
        }
    }

    func setupData() {
        guard let item = item else { return }
        
        titleLabel.text = item.title
        unitsLabel.text = item.units
    }

}
