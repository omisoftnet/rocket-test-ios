//
//  DetailsPresenter.swift
//  TemplateProject
//

import Foundation
import RxSwift

class AddItemPresenter: MvpPresenter<AddItemMvpView> {
    
    func backButtonPressed() {
        mvpView?.backToPrevScreen(with: nil)
    }

    func addItemButtonPressed() {
        guard let item = mvpView?.getItem() else {
            return
        }
        let context = RouteContext([MainViewPresenter.ArgItem: item])
        mvpView?.backToPrevScreen(with: context)
    }
}
