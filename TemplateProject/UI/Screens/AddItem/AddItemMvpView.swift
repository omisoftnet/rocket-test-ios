//
//  DetailsMvpView.swift
//  TemplateProject
//
//  Created by Dmytro Romaniuk on 9/14/18.
//  Copyright © 2018 omisoft. All rights reserved.
//

import Foundation

protocol AddItemMvpView: MvpView {
    func getItem() -> ItemModel?
}
