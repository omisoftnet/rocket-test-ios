//
//  DetailsVC.swift
//  TemplateProject
//

import UIKit
import TagListView

class AddItemVC: BaseVC<AddItemPresenter> {

    @IBOutlet weak var itemNameTextField: UITextField!
    @IBOutlet var unitsButtons: [UIButton]!
    @IBOutlet weak var customUnitsTextField: UITextField!
    @IBOutlet weak var addItemButton: UIButton!

    private var selectedUnits: Units = Units.Kilograms

    override func initPresenter(with context: RouteContext?) {
        presenter = AddItemPresenter()
        presenter?.attachView(mvpView: self)
        presenter?.setContext(to: context)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customUnitsTextField.isHidden = true
        unitButtonPressed(button: unitsButtons.first!)
    }
    
    override func setupStyle() {
        super.setupStyle()
        addItemButton <- [Style.cornersMedium]
        for button in unitsButtons {
            button <- [Style.cornersSmall]
        }
    }

    private func unitButtonPressed(button: UIButton) {
        for button in unitsButtons {
            button.backgroundColor = UIColor.white
            button.setTitleColor(UIColor.darkGray, for: .normal)
        }
        button.backgroundColor = UIColor(red: 55/255, green: 151/255, blue: 255/255, alpha: 1)
        button.setTitleColor(UIColor.white, for: .normal)
        let buttonIndex = unitsButtons.firstIndex(of: button) ?? 0
        selectedUnits = Units(rawValue: buttonIndex) ?? Units.Kilograms
        customUnitsTextField.isHidden = selectedUnits != Units.Custom
        view.layoutSubviews()

    }

    @IBAction func backButtonPressed(_ sender: Any) {
        presenter?.backButtonPressed()
    }

    @IBAction func addItemButtonPressed(_ sender: Any) {
        presenter?.addItemButtonPressed()
    }

    @IBAction func unitsButtonsPressed(_ sender: UIButton) {
        unitButtonPressed(button: sender)
    }

}

// MARK: - MVP view
extension AddItemVC: AddItemMvpView {
    func getItem() -> ItemModel? {
        guard let name = itemNameTextField.text, !name.isEmpty else {
            return nil
        }

        guard !(selectedUnits == .Custom && customUnitsTextField.text?.isEmpty ?? true) else {
            return nil
        }

        guard let units = selectedUnits.title ?? customUnitsTextField.text else {
            return nil
        }
        
        let item = ItemModel(title: name, units: units)
        return item
    }
}
