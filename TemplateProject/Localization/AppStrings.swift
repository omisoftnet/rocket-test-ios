//
//  AppStrings.swift
//  MKClient
//
//  Created by Dmytro Romaniuk on 8/27/18.
//  Copyright © 2018 omisoft. All rights reserved.
//

import Foundation
import UIKit

enum AppString: String {
    
    // MARK: common
    case ok = "ok"
    case pleaseWait = "please_wait"
    
    // MARK: errors
    case unknownError = "unknown_error"
    case timeoutError = "timeout_error"
    case unauthorizedError = "unauthorized_error"
    case networkError = "network_error"
    
    var localized: String {
        return self.rawValue.localized()
    }
}

extension String {
    func localized(bundle: Bundle = .main, tableName: String = "Localizable") -> String {
        return NSLocalizedString(self, tableName: tableName, value: "**\(self)**", comment: "")
    }
}
