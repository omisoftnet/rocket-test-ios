//
//  Logger.swift
//  TemplateProject
//
//  Created by Dmytro Romaniuk on 9/14/18.
//  Copyright © 2018 omisoft. All rights reserved.
//

import Foundation

class Logger {
    
    private static var enable = true
    
    static func configure() {
        #if DEBUG
        enable = true
        #else
        enable = false
        #endif
    }
    
}

enum MessageType: String {
    case verbose = "🖤 VERBOSE"
    case debug = "💙 DEBUG"
    case info = "💚 INFO"
    case warn = "🧡 WARN"
    case error = "❤️ ERROR"
}

// TODO maybe like log.info(), log.debug() ...
// and add visualizing adding logger - constructor
// or https://github.com/nilsleiffischer/Evergreen

func log(_ message: String?, for type: MessageType = .info) {
    print("\(type.rawValue) - \(message ?? "")")
}

func log(_ error: Error) {
    log(error.localizedDescription, for: .error)
}
