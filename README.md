## Rocket Test iOS
Test application based on our MVP Architecture template.

## Preview
<img src="./Demo/sc1.png" alt="preview screenshot 1"  width="200">
<img src="./Demo/sc2.png" alt="preview screenshot 2"  width="200">
